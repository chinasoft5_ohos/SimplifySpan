# SimplifySpan

#### 项目介绍
- 项目名称：SimplifySpan
- 所属系列：openharmony的第三方组件适配移植
- 功能：简化的Spanner库
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 2.2
#### 效果演示 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0623/173726_15007f7d_1264960.gif)

#### 安装教程
方式一：

1.在项目根目录下的build.gradle文件中，

 ```

allprojects 

    repositories {

       maven {
                   url 'https://s01.oss.sonatype.org/content/repositories/releases/'
               }

    }

}

 ```

2.在entry模块的build.gradle文件中，

 ```

 dependencies {

        implementation('com.gitee.chinasoft_ohos:SimplifySpan:1.0.0')

    ......  

 }

 ```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下
#### 使用方式
```
RichTextUtils richTextCu = new RichTextUtils(textHahaha, new SpecialTextUnit("正常字体", "张")
                .useTextBold().setTextSize(45).setTextColor(0xFFFFA500));


        textHahaha.setRichText(richTextCu
                .append(new SpecialTextUnit("粗体").useTextBold())
                .append(new SpecialTextUnit("斜体删除线").useTextItalic().showStrikeThrough())
                .append(new SpecialTextUnit("粗斜体黑体").useTextBold().setTextBackgroundColor(Color.RED.getValue())
                        .setTextColor(Color.RED.getValue()))
                .append(new SpecialTextUnit("等宽字体Sans Serif字体Serif字体").setFont(Font.SANS_SERIF))
                .append(new SpecialTextUnit("居中").setTextColor(Color.BLUE.getValue())
                        .setTextSize(25))
                .append(new SpecialTextUnit("正常")
                        .setTextSize(50).setTextColor(Color.RED.getValue()).setGravity(SpecialGravity.TOP))

                .append(new SpecialTextUnit("底部").setGravity(SpecialGravity.BOTTOM).setTextSize(25))
                .build());


        SpecialTextUnit specialTextUnit = new SpecialTextUnit("@英雄联盟");
        specialTextUnit.setTextColor(Color.BLUE.getValue())
                .showUnderline()
                .setClickLisenter(new OnTextClickLisenter() {
                    @Override
                    public void onClick(Object clickContent) {
                        show((String) clickContent);
                    }
                });
```
#### 测试信息
```
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

```
#### 版本迭代

- 1.0.0
- 0.0.1-SNAPSHOT

#### 版权和许可信息
```
The MIT License (MIT)

Copyright (c) 2015 iWgang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```





