package cn.iwgang.simplifyspan.unit;

import cn.iwgang.simplifyspan.other.SpecialConvertMode;
import ohos.agp.text.Font;

/**
 * 特殊字体.
 *
 * @author wl
 * @since 2021-06-24
 */
public class SpecialTextUnit extends BaseSpecialUnit {
    private int textColor;
    private int textSize;
    private Font font;
    private boolean isTextItalic;
    private int convertMode;
    /**
     * 字体背景颜色
     */
    private int textBackgroundColor;

    public Font getFont() {
        return font;
    }

    public int getGravity() {
        return gravity;
    }

    /**
     * 位置
     */
    private int gravity;
    /**
     * 是否显示删除线
     */
    private boolean isShowStrikeThrough;
    /**
     * 是否显示下划线
     */
    private boolean isShowUnderline;
    /**
     * 是否用粗体
     */
    private boolean isTextBold;

    /**
     * 传一个初始文本时 设置的点击事件默认是整个区域
     *
     * @param text text
     */
    public SpecialTextUnit(String text) {
        super(text);
    }

    /**
     * 构建
     *
     * @param startText String
     * @param text String
     * @return SpecialTextUnit
     */
    public SpecialTextUnit(String startText, String text) {
        super(startText, text);
    }

    /**
     * @param text      text
     * @param textColor Text Color
     * @return SpecialTextUnit
     */
    public SpecialTextUnit(String text, int textColor) {
        super(text);
        this.textColor = textColor;
    }

    /**
     * @param text      text
     * @param textColor text color
     * @param textSize  text size (unit：sp)
     * @return SpecialTextUnit
     */
    public SpecialTextUnit(String text, int textColor, int textSize) {
        this(text);
        this.textColor = textColor;
        this.textSize = textSize;
    }

    /**
     * Show StrikeThrough
     *
     * @return SpecialTextUnit
     */
    public SpecialTextUnit showStrikeThrough() {
        isShowStrikeThrough = true;
        return this;
    }

    /**
     * Show Underline
     *
     * @return SpecialTextUnit
     */
    public SpecialTextUnit showUnderline() {
        isShowUnderline = true;
        return this;
    }

    /**
     * Use Text Bold
     *
     * @return SpecialTextUnit
     */
    public SpecialTextUnit useTextBold() {
        isTextBold = true;
        return this;
    }

    /**
     * Use Text Bold
     *
     * @return SpecialTextUnit
     */
    public SpecialTextUnit useTextItalic() {
        isTextItalic = true;
        return this;
    }


    /**
     * Set Background Color
     *
     * @param textBackgroundColor color
     * @return SpecialTextUnit
     */
    public SpecialTextUnit setTextBackgroundColor(int textBackgroundColor) {
        this.textBackgroundColor = textBackgroundColor;
        return this;
    }

    /**
     * Set Special Text Color
     *
     * @param textColor color
     * @return SpecialTextUnit
     */
    public SpecialTextUnit setTextColor(int textColor) {
        this.textColor = textColor;
        return this;
    }

    /**
     * Set Special Text Size
     *
     * @param textSize size (sp)
     * @return SpecialTextUnit
     */
    public SpecialTextUnit setTextSize(int textSize) {
        this.textSize = textSize;
        return this;
    }

    /**
     * Set Gravity
     *
     * @param gravity use SpecialGravity.xx
     * @return SpecialTextUnit
     */
    public SpecialTextUnit setGravity(int gravity) {
        this.gravity = gravity;
        return this;
    }

    /**
     * Set Convert Mode
     *
     * @param convertMode use SpecialConvertMode.xx
     * @return SpecialTextUnit
     */
    public SpecialTextUnit setConvertMode(int convertMode) {
        this.convertMode = convertMode;
        return this;
    }

    /**
     * 获取替换的方式
     * 默认是替换全部
     *
     * @return int
     */
    public int getConvertMode() {
        return convertMode == 0 ? SpecialConvertMode.ALL : convertMode;
    }

    /**
     * setFont
     *
     * @param font Font
     * @return SpecialTextUnit
     */
    public SpecialTextUnit setFont(Font font) {
        this.font = font;
        return this;
    }

    /**
     * getTextColor
     *
     * @return int
     */
    public int getTextColor() {
        return textColor;
    }

    /**
     * getTextBackgroundColor
     *
     * @return int
     */
    public int getTextBackgroundColor() {
        return textBackgroundColor;
    }

    /**
     * getTextSize
     *
     * @return int
     */
    public int getTextSize() {
        return textSize;
    }

    /**
     * isShowUnderline
     *
     * @return boolean
     */
    public boolean isShowUnderline() {
        return isShowUnderline;
    }

    /**
     * isShowStrikeThrough
     *
     * @return boolean
     */
    public boolean isShowStrikeThrough() {
        return isShowStrikeThrough;
    }

    /**
     * isTextBold
     *
     * @return boolean
     */
    public boolean isTextBold() {
        return isTextBold;
    }

    /**
     * isTextItalic
     *
     * @return boolean
     */
    public boolean isTextItalic() {
        return isTextItalic;
    }
}
