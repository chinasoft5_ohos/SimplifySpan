package cn.iwgang.simplifyspan.unit;

import cn.iwgang.simplifyspan.other.OnClickableSpanListener;
import ohos.agp.utils.TextTool;

import java.util.ArrayList;

/**
 * ex.
 *
 * @author wl
 * @since 2021-06-24
 */
public class BaseSpecialUnit {
    private String text;
    private String startStr;
    private ArrayList<OnClickableSpanListener> clickLisenter;
    private ArrayList<String> clickStr;

    /**
     * BaseSpecialUnit
     *
     * @param text String
     * @return BaseSpecialUnit
     */
    public BaseSpecialUnit(String text) {
        this.text = text;
        this.startStr = text;
        init();
    }

    /**
     * BaseSpecialUnit
     *
     * @param startStr String
     * @param text     String
     * @return BaseSpecialUnit
     */
    public BaseSpecialUnit(String startStr, String text) {
        this.text = text;
        this.startStr = startStr;
        init();
    }

    private void init() {
        clickLisenter = new ArrayList<>();
        clickStr = new ArrayList<>();
    }

    /**
     * getText
     *
     * @return String
     */
    public String getText() {
        return text;
    }

    /**
     * getStartText
     *
     * @return String
     */
    public String getStartText() {
        return startStr;
    }

    /**
     * setClickLisenter
     *
     * @param lisenter  OnClickableSpanListener
     * @param clickText String
     * @return BaseSpecialUnit
     */
    public BaseSpecialUnit setClickLisenter(OnClickableSpanListener lisenter, String clickText) {
        if (clickLisenter != null && clickStr != null) {
            clickLisenter.add(lisenter);
            if (!TextTool.isNullOrEmpty(clickText)) {
                clickStr.add(text);
            }
        }
        return this;
    }

    /**
     * setClickLisenter
     *
     * @param lisenter OnClickableSpanListener
     * @return BaseSpecialUnit
     */
    public BaseSpecialUnit setClickLisenter(OnClickableSpanListener lisenter) {
        if (clickLisenter != null && clickStr != null) {
            clickLisenter.add(lisenter);
            if (!TextTool.isNullOrEmpty(text)) {
                clickStr.add(text);
            }
        }
        return this;
    }

    /**
     * getClickLisenter
     *
     * @return ArrayList<OnTextClickLisenter>
     */
    public ArrayList<OnClickableSpanListener> getClickLisenter() {
        return clickLisenter;
    }

    /**
     * getClickStr
     *
     * @return ArrayList<String>
     */
    public ArrayList<String> getClickStr() {
        return clickStr;
    }


}
