package cn.iwgang.simplifyspan.other;

/**
 * ex.
 *
 * @author wl
 * @since 2021-06-24
 */
public interface OnClickableSpanListener {
    /**
     * 文本点击事件的回调
     *
     * @param clickContent 被点击的文本
     */
    void onClick(Object clickContent);
}
