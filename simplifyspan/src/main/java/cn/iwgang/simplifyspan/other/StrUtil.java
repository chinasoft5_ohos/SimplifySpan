/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.iwgang.simplifyspan.other;

import java.util.List;

/**
 * ex.
 *
 * @author wl
 * @since 2021-06-24
 */
public class StrUtil {

    /**
     * 判断 splitStr  在 initStr 行尾出现过几次
     *
     * @param initStr initStr
     * @param splitStr splitStr
     * @return int
     */
    public static int checkStrEndFoundCount(String initStr, String splitStr) {
        int count = 0;
        int lastIndex = initStr.lastIndexOf(splitStr);
        String newStr = initStr;
        while (lastIndex >= newStr.length() - splitStr.length()) {
            count++;
            lastIndex = newStr.lastIndexOf(splitStr);
            newStr = newStr.substring(0, lastIndex);
            lastIndex = newStr.lastIndexOf(splitStr);
        }
        return count;
    }

    /**
     * isListEmpty
     *
     * @param list list
     * @return boolean
     */
    public static boolean isListEmpty(List list) {
        if (list != null && list.size() != 0) {
            return false;
        }
        return true;
    }
}
