package cn.iwgang.simplifyspan.other;

/**
 * Special Convert Mode
 * Created by iWgang on 15/12/3.
 * https://github.com/iwgang/SimplifySpan
 */
public class SpecialConvertMode {

    /**
     * 替换中只替换相同字符串中的首个
     */
    public static final int ONLY_FIRST = 1;

    /**
     * 替换中只替换相同字符串中的最后一个
     */
    public static final int ONLY_LAST = 2;

    /**
     * 替换中替换所有相同字符串
     */
    public static final int ALL = 3;
}
