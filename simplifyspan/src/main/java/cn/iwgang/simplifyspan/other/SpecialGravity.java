package cn.iwgang.simplifyspan.other;

/**
 * Special Gravity
 * Created by iWgang on 15/12/3.
 * https://github.com/iwgang/SimplifySpan
 */
public class SpecialGravity {

    /**
     * 上
     */
    public static final int TOP = 1;

    /**
     * 中
     */
    public static final int CENTER = 2;

    /**
     * 下
     */
    public static final int BOTTOM = 3;
}
