/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.iwgang.simplifyspandemo;

import cn.iwgang.simplifyspan.other.StrUtil;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * ex.
 *
 * @author wl
 * @since 2021-06-24
 */
public class StringUtilTestCase extends TestCase {

    /**
     * testListIsEmpty
     */
    @Test
    public void testListIsEmpty() {
        List<String> list = null;
        boolean listEmpty = StrUtil.isListEmpty(list);
        assertEquals(true, listEmpty);
    }

    /**
     * testListIsEmpty2
     */
    @Test
    public void testListIsEmpty2() {
        List<String> list = new ArrayList<>();
        boolean listEmpty = StrUtil.isListEmpty(list);
        assertEquals(true, listEmpty);
    }

    /**
     * testListIsNotEmpty
     */
    @Test
    public void testListIsNotEmpty() {
        List<String> list = new ArrayList<>();
        list.add("11111");
        list.add("22222");
        list.add("33333");
        boolean listEmpty = StrUtil.isListEmpty(list);
        assertEquals(false, listEmpty);
    }

}
