package cn.iwgang.simplifyspandemo;

import cn.iwgang.simplifyspandemo.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * ex.
 *
 * @author wl
 * @since 2021-06-24
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
