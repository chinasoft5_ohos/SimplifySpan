package cn.iwgang.simplifyspandemo.slice;

import cn.iwgang.simplifyspan.unit.BaseSpecialUnit;
import cn.iwgang.simplifyspandemo.ResourceTable;
import cn.iwgang.simplifyspan.SimplifySpanBuild;
import cn.iwgang.simplifyspan.other.OnClickableSpanListener;
import cn.iwgang.simplifyspan.other.SpecialConvertMode;
import cn.iwgang.simplifyspan.other.SpecialGravity;
import cn.iwgang.simplifyspan.unit.SpecialTextUnit;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * ex.
 *
 * @author wl
 * @since 2021-06-24
 */
public class MainAbilitySlice extends AbilitySlice {

    private Text text;
    private Text text1;
    private Text text2;
    private Text textHahaha;
    private Text textFinal;
    private Text textFinal2;
    private String str;
    private SpecialTextUnit specialTextUnit;
    private SpecialTextUnit specialTextUnit0;
    private SpecialTextUnit specialTextUnit1;
    private SpecialTextUnit specialTextUnit2;
    private List<BaseSpecialUnit> units;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        units = new ArrayList<>();
        text = (Text) findComponentById(ResourceTable.Id_text_helloworld);
        text1 = (Text) findComponentById(ResourceTable.Id_text_helloworld1);
        text2 = (Text) findComponentById(ResourceTable.Id_text_helloworld2);
        textHahaha = (Text) findComponentById(ResourceTable.Id_text_hahha);
        textFinal = (Text) findComponentById(ResourceTable.Id_textFinal);
        textFinal2 = (Text) findComponentById(ResourceTable.Id_textFinal2);
        str = "替换所有张字的颜色及字体大小并加粗，张歆艺、张馨予、张嘉倪、张涵予、张含韵、张韶涵、张嘉译、张佳宁、大张伟张张张";
        replaceStr(text, text1, text2, str);
        setRichText1();
        createUnit();
        setRichText2();
        units.add(new SpecialTextUnit("无法设置字体背景"));
        SimplifySpanBuild richText2 = new SimplifySpanBuild(textFinal2, units);
        richText2.append(new SpecialTextUnit("点我点我1")
                .setTextColor(Color.BLUE.getValue())
                .setClickLisenter(new OnClickableSpanListener() {
                    @Override
                    public void onClick(Object clickContent) {
                        show((String) clickContent);
                    }
                }))
                .append(new SpecialTextUnit("无默认背景显示下划线"))
                .append(new SpecialTextUnit("点我点我2")
                        .showUnderline()
                        .setTextColor(Color.BLUE.getValue())
                        .setClickLisenter(new OnClickableSpanListener() {
                            @Override
                            public void onClick(Object clickContent) {
                                show((String) clickContent);
                            }
                        }))
                .append(new SpecialTextUnit("无法设置默认背景以及点击背景"))
                .append(new SpecialTextUnit("点我点我3")
                        .showUnderline()
                        .setTextColor(Color.RED.getValue())
                        .setClickLisenter(new OnClickableSpanListener() {
                            @Override
                            public void onClick(Object clickContent) {
                                show((String) clickContent);
                            }
                        }))
                .append(new SpecialTextUnit("我只是个结尾"));
        textFinal2.setRichText(richText2.build());

    }

    private void setRichText2() {
        units.clear();
        units.add(specialTextUnit);
        units.add(specialTextUnit0);
        units.add(specialTextUnit1);
        units.add(specialTextUnit2);
        SimplifySpanBuild richTextUtils1 = new SimplifySpanBuild(textHahaha, units)
                .append(new SpecialTextUnit("星光熠熠的各赛区阵容、精彩的1V1Solo赛、克隆/" +
                        "双人共玩等奇葩套路层出不穷的花样对抗，你最期待看到哪位选手、哪种模式的对决？"))
                .append(new SpecialTextUnit("LOL新大战闻声识英雄")
                        .setTextColor(Color.BLUE.getValue())
                        .setClickLisenter(new OnClickableSpanListener() {
                            @Override
                            public void onClick(Object clickContent) {
                                show((String) clickContent);
                            }
                        }))
                .append(new SpecialTextUnit("完整文章见 "))
                .append(new SpecialTextUnit("LOL超强攻略,不看绝对后悔 ")
                        .setTextColor(Color.GREEN.getValue())
                        .setClickLisenter(new OnClickableSpanListener() {
                            @Override
                            public void onClick(Object clickContent) {
                                show((String) clickContent);
                            }
                        }))
                .append(new SpecialTextUnit(" 更多好玩的内容请点击 "))
                .append(new SpecialTextUnit(" 网页链接 ")
                        .setTextColor(Color.GREEN.getValue())
                        .setClickLisenter(new OnClickableSpanListener() {
                            @Override
                            public void onClick(Object clickContent) {
                                show((String) clickContent);
                            }
                        }))
                .append(new SpecialTextUnit(" 已收录 "))
                .append(new SpecialTextUnit(" LOL新闻库 ").setTextColor(Color.BLUE.getValue())
                        .setClickLisenter(new OnClickableSpanListener() {
                            @Override
                            public void onClick(Object clickContent) {
                                show((String) clickContent);
                            }
                        }))
                .append(new SpecialTextUnit(" 后面加的内容是为了凑字数的哈"));
        textFinal.setRichText(richTextUtils1.build());
    }

    private void createUnit() {
        specialTextUnit = new SpecialTextUnit("@英雄联盟");
        specialTextUnit.setTextColor(Color.BLUE.getValue())
                .showUnderline()
                .setClickLisenter(new OnClickableSpanListener() {
                    @Override
                    public void onClick(Object clickContent) {
                        show((String) clickContent);
                    }
                });
        specialTextUnit0 = new SpecialTextUnit("：");
        specialTextUnit1 = new SpecialTextUnit("#LOG夜话# ");
        specialTextUnit1.setTextColor(Color.BLUE.getValue()).setClickLisenter(new OnClickableSpanListener() {
            @Override
            public void onClick(Object clickContent) {
                show((String) clickContent);
            }
        });
        specialTextUnit2 = new SpecialTextUnit("#LOL明星召唤师#", "明星召唤师");
        specialTextUnit2.setTextColor(Color.RED.getValue()).setClickLisenter(new OnClickableSpanListener() {
            @Override
            public void onClick(Object clickContent) {
                show((String) clickContent);
            }
        }, "明星召唤师");
    }

    private void setRichText1() {
        units.clear();
        units.add(new SpecialTextUnit("正常字体", "张")
                .useTextBold()
                .setTextSize(45)
                .setTextColor(0xFFFFA500));
        SimplifySpanBuild richTextCu = new SimplifySpanBuild(textHahaha, units);
        textHahaha.setRichText(richTextCu
                .append(new SpecialTextUnit("粗体").useTextBold())
                .append(new SpecialTextUnit("斜体删除线").useTextItalic().showStrikeThrough())
                .append(new SpecialTextUnit("粗斜体黑体").useTextBold().setTextBackgroundColor(Color.RED.getValue())
                        .setTextColor(Color.RED.getValue()))
                .append(new SpecialTextUnit("等宽字体Sans Serif字体Serif字体").setFont(Font.SANS_SERIF))
                .append(new SpecialTextUnit("居中").setTextColor(Color.BLUE.getValue())
                        .setTextSize(25))
                .append(new SpecialTextUnit("正常")
                        .setTextSize(50).setTextColor(Color.RED.getValue()).setGravity(SpecialGravity.TOP))
                .append(new SpecialTextUnit("底部").setGravity(SpecialGravity.BOTTOM).setTextSize(25))
                .build());
    }

    private SimplifySpanBuild replaceStr(Text text, Text text1, Text text2, String str) {
        units.clear();
        units.add(new SpecialTextUnit(str, "张")
                .setConvertMode(SpecialConvertMode.ALL)
                .useTextBold()
                .setTextColor(0xFFFFA500)
                .setClickLisenter(new OnClickableSpanListener() {
                    @Override
                    public void onClick(Object clickContent) {
                        show((String) clickContent);
                    }
                }, "张"));
        SimplifySpanBuild richTextUtils = new SimplifySpanBuild(text, units);
        units.clear();
        units.add(new SpecialTextUnit(str, "张")
                .setConvertMode(SpecialConvertMode.ONLY_FIRST)
                .useTextBold()
                .setTextColor(0xFFFFA500)
                .setClickLisenter(new OnClickableSpanListener() {
                    @Override
                    public void onClick(Object clickContent) {
                        show((String) clickContent);
                    }
                }, "张"));
        SimplifySpanBuild richTextUtils1 = new SimplifySpanBuild(text, units);
        units.clear();
        units.add(new SpecialTextUnit(str, "张")
                .setConvertMode(SpecialConvertMode.ONLY_LAST)
                .useTextBold()
                .setTextColor(0xFFFFA500)
                .setClickLisenter(new OnClickableSpanListener() {
                    @Override
                    public void onClick(Object clickContent) {
                        show((String) clickContent);
                    }
                }, "张"));
        SimplifySpanBuild richTextUtils2 = new SimplifySpanBuild(text, units);
        text.setRichText(richTextUtils.build());
        text1.setRichText(richTextUtils1.build());
        text2.setRichText(richTextUtils2.build());
        return richTextUtils;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /**
     * 显示toast
     *
     * @param msg msg
     */
    public void show(String msg) {
        new ToastDialog(MainAbilitySlice.this)
                .setText((String) msg)
                .setDuration(200)
                .show();
    }
}
